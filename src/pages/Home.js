import React from "react";
import { Link } from "react-router-dom";

const Home = () => {
	return (
	   <>
	     <section classname="home-wrapper-1 py-5">
	        <div classname="container-xxl">
	           <div classname="row">
	              <div classname="col-6">
	                 <div classname="main-banner position-relative p-3">
	                    <img src="images/main-banner-1.jpg" classname="img-fluid rounded-3" alt="main banner"/>
	                    <div classname="main-banner-content position-absolute">
	                    <h4>SUPERCHARGED FOR PROS.</h4>
	                    <h5>iPad S13+ Pro.</h5>
	                    <p>FROM $999.00 or $41.62/mo</p>
	                    <Link>BUY NOW</Link>
	                    </div>
	                 </div>
	              </div>
	              <div classname="col-6"></div>
	           </div>
	        </div>
	     </section>
	   </>
	);
}

export default Home;