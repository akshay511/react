import React from "react";
import { PRODUCTS } from "../../products" ;
import { Products } from "./products";
import './shop.css';
export const Shop = () =>{
	return( 
		<div className="shop">
			<div className="shopTitle">
				<h1>Shopping</h1>
			</div>
			<br/>
			<div className="products">
				{ PRODUCTS.map((products) => (
					<Products data={products}/>
				))}
			</div>
		</div>
	)
}