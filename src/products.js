import product1 from './assets/products/laptop.jpeg';
import product2 from './assets/products/iphone.jpeg';
import product3 from './assets/products/camera.jpg';
import product4 from './assets/products/cap.jpeg';
import product5 from './assets/products/cap1.jpeg';
import product6 from './assets/products/shirt.jpeg';
import product7 from './assets/products/t-shirt.webp';
export const PRODUCTS = [
    {
       id: 1,
       productName: "laptop",
       price: 90000.0,
       productImage: product1,
    },
    {
       id: 2,
       productName: "IPhone",
       price: 99999.0,
       productImage: product2,
    },
    {
       id: 3,
       productName: "camera",
       price: 99994.0,
       productImage: product3,
    },
    {
       id: 4,
       productName: "Cap",
       price: 399.0,
       productImage: product4,
    },
    {
       id: 5,
       productName: "Cap blue",
       price: 299.0,
       productImage: product5,
    },
    {
       id: 6,
       productName: "Shirt",
       price: 599.0,
       productImage: product6,
    },
    {
       id: 7,
       productName: "t-shirt",
       price: 699.0,
       productImage: product7,
    },

]